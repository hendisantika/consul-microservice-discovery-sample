package com.hendisantika.cloud.portofolio;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class PortofolioApplication {

    public static void main(String[] args) {
        SpringApplication.run(PortofolioApplication.class, args);
    }

}

